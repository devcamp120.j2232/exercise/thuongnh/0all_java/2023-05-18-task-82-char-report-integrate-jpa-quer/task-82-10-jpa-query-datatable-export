package com.devcamp.pizza365.service;

import java.util.List;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import com.devcamp.pizza365.entity.Customer;

/**
 * @author HieuHN
 *
 */
public class ExcelExporter {

	// 1 khai báo các thuộc tính
	// 2 Constructor khởi tạo server export danh sách Customer 
	// 3  Tạo các ô cho excel file.
	// 4 Khai báo cho sheet và các dòng đầu tiên / dòng tiêu đề
	// 5 fill dữ liệu cho các dòng tiếp theo.
	// 6  xuất dữ liệu ra dạng file

	// khai báo các thuộc tính
	private XSSFWorkbook workbook;   // là một class trong thư viện Apache POI, được sử dụng để tạo workbook (tài liệu Excel).
	private XSSFSheet sheet;  // Dòng này khai báo một biến private sheet kiểu XSSFSheet. XSSFSheet là một class trong thư viện Apache POI, được sử dụng để tạo sheet (trang tính Excel).
	private List<Customer> customers;  // à một danh sách các đối tượng Customer

	/**
	 * Constructor khởi tạo server export danh sách Customer 
	 * @param customers
	 */
	public ExcelExporter(List<Customer> customers) {
		this.customers = customers;
		this.workbook = new XSSFWorkbook();
	}

	/**
	 * Tạo các ô cho excel file.
	 * @param row
	 * @param columnCount
	 * @param value
	 * @param style
	 */
	private void createCells(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);  //Dòng này thực hiện điều chỉnh kích thước của cột trong sheet để phù hợp với nội dung của ô.
		Cell cell = row.createCell(columnCount); // columnCount là số thứ tự của ô cần tạo 
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);  // nếu là kiểu Integer thì gán giá trị của ô bằng cell.setCellValue((Integer) value)
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);  // Dòng này thiết lập kiểu của ô bằng giá trị được truyền vào từ tham số style.

	}

	/**
	 * Khai báo cho sheet và các dòng đầu tiên / dòng tiêu đề
	 */
	private void writeHeaderLine() {
		this.sheet = workbook.createSheet("Customers");  //Nó tạo một sheet mới trong workbook (được khai báo ở đầu class) và đặt tên của sheet là "Customers".
		// ở đây là hàng đầu tiên của sheet. Hàng này được lưu trữ trong biến "row" để sử dụng cho việc thêm các ô tiêu đề cho các cột.
		Row row = this.sheet.createRow(0);   
		// Nó tạo một đối tượng CellStyle mới, đại diện cho các thuộc tính và phong cách của các ô trong sheet.
		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();  // Nó tạo một đối tượng XSSFFont mới để thiết lập font chữ và cỡ chữ cho các ô trong sheet.

		font.setBold(true);  // chữ đậm 
		font.setFontHeight(16);  // size  16   
		style.setFont(font);   // Dòng này được sử dụng để thiết lập font cho style bằng phương thức setFont()
		// để tạo các ô cho các cột, với các thông số truyền vào lần lượt là:
		createCells(row, 0, "User ID", style);
		createCells(row, 1, "Phone", style);
		createCells(row, 2, "Full Name", style);
		createCells(row, 3, "Address", style);
		createCells(row, 4, "Sale", style);
		createCells(row, 5, "Action", style);
	}
	/**
	 * fill dữ liệu cho các dòng tiếp theo.
	 */
	private void writeDataLines() {
		int rowCount = 1;

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
		style.setFont(font);  //  set style
		// customers là array list 
		for (Customer user : this.customers) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;
			
			createCells(row, columnCount++, user.getId(), style);
			createCells(row, columnCount++, user.getPhoneNumber(), style);
			createCells(row, columnCount++, user.getFirstName() + ", " + user.getLastName(), style);
			createCells(row, columnCount++, user.getAddress(), style);
			createCells(row, columnCount++, user.getSalesRepEmployeeNumber(), style);
			createCells(row, columnCount++, "ABC", style);

		}
	}

	/**
	 * xuất dữ liệu ra dạng file
	 * @param response
	 * @throws IOException
	 */
	// . Nếu có lỗi xảy ra trong quá trình xuất file, phương thức sẽ ném ra ngoại lệ IOException.
	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();

	}
}
