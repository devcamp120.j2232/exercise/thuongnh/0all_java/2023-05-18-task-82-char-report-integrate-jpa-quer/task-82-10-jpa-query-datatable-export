package com.devcamp.pizza365.serviceEntity;
import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.model.ExistsData;
import com.devcamp.pizza365.repository.EntityRepository.IEmployeeRepository;

@Service
public class EmployeeEntityService {
    @Autowired
	IEmployeeRepository gEmployeeRepository;

	public ResponseEntity<List<Employee>> getAllEmployee() {
		try {
			List<Employee> vEmployees = new ArrayList<Employee>();
			gEmployeeRepository.findAll().forEach(vEmployees::add);
			return new ResponseEntity<>(vEmployees, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	  // 1056
	public ResponseEntity<Object> getEmployeeById(@PathVariable Integer id) {
		Optional<Employee> vEmployeeData = gEmployeeRepository.findById(id);
		if (vEmployeeData.isPresent()) {
			try {
				Employee vEmployee = vEmployeeData.get();
				return new ResponseEntity<>(vEmployee, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Employee vEmployeeNull = new Employee();
			return new ResponseEntity<>(vEmployeeNull, HttpStatus.NOT_FOUND);
		}
	}



	public ResponseEntity<Object> createEmployee(@Valid @RequestBody Employee paramEmployee) {
		try {
			Employee vEmployee = new Employee();
			vEmployee.setLastName(paramEmployee.getLastName());
			vEmployee.setFirstName(paramEmployee.getFirstName());
			vEmployee.setExtension(paramEmployee.getExtension());
			vEmployee.setEmail(paramEmployee.getEmail());
			vEmployee.setOfficeCode(paramEmployee.getOfficeCode());
			vEmployee.setReportTo(paramEmployee.getReportTo());
			vEmployee.setJobTitle(paramEmployee.getJobTitle());
			Employee vEmployeeSave = gEmployeeRepository.save(vEmployee);
			return new ResponseEntity<>(vEmployeeSave, HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Employee: " + e.getCause().getCause().getMessage());
		}
	}

	public ResponseEntity<Object> updateEmployee(@PathVariable Integer id, @Valid @RequestBody Employee paramEmployee) {
		Optional<Employee> vEmployeeData = gEmployeeRepository.findById(id);
		if (vEmployeeData.isPresent()) {
			try {
				Employee vEmployee = vEmployeeData.get();
				vEmployee.setLastName(paramEmployee.getLastName());
				vEmployee.setFirstName(paramEmployee.getFirstName());
				vEmployee.setExtension(paramEmployee.getExtension());
				vEmployee.setEmail(paramEmployee.getEmail());
				vEmployee.setOfficeCode(paramEmployee.getOfficeCode());
				vEmployee.setReportTo(paramEmployee.getReportTo());
				vEmployee.setJobTitle(paramEmployee.getJobTitle());
				Employee vEmployeeSave = gEmployeeRepository.save(vEmployee);
				return new ResponseEntity<>(vEmployeeSave, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Employee: " + e.getCause().getCause().getMessage());
			}

		} else {
			Employee vEmployeeNull = new Employee();
			return new ResponseEntity<>(vEmployeeNull, HttpStatus.NOT_FOUND);
		}
	}

    public	ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
		Optional<Employee> vEmployeeData = gEmployeeRepository.findById(id);
		if (vEmployeeData.isPresent()) {
			try {
				gEmployeeRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Employee vEmployeeNull = new Employee();
			return new ResponseEntity<>(vEmployeeNull, HttpStatus.NOT_FOUND);
		}
	}
	public ResponseEntity<Object> isExistsEmail(@PathVariable String email) {
		try {
			boolean vEmployee = gEmployeeRepository.existsByEmail(email);
			ExistsData vExistsData = new ExistsData();
			if (vEmployee) {
				vExistsData.setName(email);
				vExistsData.setExists(true);
			} else {
				vExistsData.setName(email);
				vExistsData.setExists(false);
			}
			return new ResponseEntity<>(vExistsData, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
