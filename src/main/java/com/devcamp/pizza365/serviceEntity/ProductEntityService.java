package com.devcamp.pizza365.serviceEntity;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.repository.EntityRepository.IproductsRepository;

@Service
public class ProductEntityService {
    @Autowired
    private IproductsRepository pIproductsRepository;

    public ResponseEntity<Object> getAllProduct() {
        try {
            return new ResponseEntity<>(pIproductsRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    public ResponseEntity<Object> getProductById(@PathVariable Long id) {
        try {
            Optional<Product> newProduct = pIproductsRepository.findById(id);
            return new ResponseEntity<>(newProduct, HttpStatus.OK);
            // return new ResponseEntity<>(pIproductsRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // JSON mẫu
    // "productCode": "S10_1678",
    // "productName": "1969 Harley Davidson Ultimate Chopper",
    // "productDescripttion": "This replica features working kickstand, front
    // suspension, gear-shift lever, footbrake lever, drive chain, wheels and
    // steering. All parts are particularly delicate due to their precise scale and
    // require special care and attention.",
    // "idProductLine": 2,
    // "productScale": "1:10",
    // "productVendor": "Min Lin Diecast",
    // "quantityInStock": 7933,
    // "buyPrice": 48.81

    public ResponseEntity<Object> createProduct( Product paramProduct) {
        try {
            Product newProduct = new Product();
            newProduct.setBuyPrice(paramProduct.getBuyPrice());
            newProduct.setIdProductLine(paramProduct.getIdProductLine());
            newProduct.setOrderDetails(paramProduct.getOrderDetails());
            newProduct.setProductCode(paramProduct.getProductCode());
            newProduct.setProductDescripttion(paramProduct.getProductDescripttion());
            newProduct.setProductLine(paramProduct.getProductLine());
            newProduct.setProductName(paramProduct.getProductName());
            newProduct.setProductScale(paramProduct.getProductScale());
            newProduct.setProductVendor(paramProduct.getProductVendor());
            newProduct.setQuantityInStock(paramProduct.getQuantityInStock());

            Product savedProduct = pIproductsRepository.save(newProduct);
            return new ResponseEntity<>(savedProduct, HttpStatus.CREATED);
        } catch (Exception e) {
            // return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Product: " + e.getCause().getCause().getMessage());
        }
    }

    public ResponseEntity<Object> updateProduct( Long id, Product paramProduct) {
        Optional<Product> ProductData = pIproductsRepository.findById(id);
        if (ProductData.isPresent()) {
            Product newProduct = ProductData.get();

            newProduct.setBuyPrice(paramProduct.getBuyPrice());
            newProduct.setIdProductLine(paramProduct.getIdProductLine());
            newProduct.setOrderDetails(paramProduct.getOrderDetails());
            newProduct.setProductCode(paramProduct.getProductCode());
            newProduct.setProductDescripttion(paramProduct.getProductDescripttion());
            newProduct.setProductLine(paramProduct.getProductLine());
            newProduct.setProductName(paramProduct.getProductName());
            newProduct.setProductScale(paramProduct.getProductScale());
            newProduct.setProductVendor(paramProduct.getProductVendor());
            newProduct.setQuantityInStock(paramProduct.getQuantityInStock());

            Product savedProduct = pIproductsRepository.save(newProduct);
            return new ResponseEntity<>(savedProduct, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Product: " + id);
        }
    }

    public ResponseEntity<Object> deleteProductById(@PathVariable Long id) {
        try {
            pIproductsRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println("==================" + e.getCause().getCause().getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
   
}
