package com.devcamp.pizza365.serviceEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.EntityRepository.IProductLinesRepository;

@Service
public class ProductLineEntitySevice {

    @Autowired
    IProductLinesRepository productLinesRepository;

    public ResponseEntity<Object> getAllDrink() {
        try {
            List<ProductLine> vDrink = new ArrayList<ProductLine>(); // khởi tạo list chứa đối tượng
            productLinesRepository.findAll().forEach(vDrink::add);
            return new ResponseEntity<>(vDrink, HttpStatus.OK);

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> getById(@PathVariable Long id) {

        try {
            Optional<ProductLine> productLine = productLinesRepository.findById(id);
            if (productLine != null)
                return new ResponseEntity<>(productLine, HttpStatus.OK);
            else
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        } catch (Exception e) {
            System.out.println("Exception mới nhất ");
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    // create mẫu json thử
    // {
    // "id": 1,
    // "productLine": "Classic Cars",
    // "description": "Attention car enthusiasts: Make your wildest car ownership"
    // }
    public ResponseEntity<ProductLine> getCreate(@RequestBody ProductLine paramProductLine) {
        try {
            ProductLine _menu = productLinesRepository.save(paramProductLine);
            return new ResponseEntity<>(_menu, HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Exception mới nhất ");
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // update
    public ResponseEntity<Object> getUpdate(@PathVariable("id") long id,
            @RequestBody ProductLine pProductLine) {

        Optional<ProductLine> _product = productLinesRepository.findById(id);
        // kiểm tra có null hay k ,, true là khác null
        if (_product.isPresent()) {
            // get(); là của Optional để lấy giá tri của đối tượng
            ProductLine _productNew = _product.get(); // lấy giá trị của _menu gán cho _user
            _productNew.setDescription(pProductLine.getDescription());
            _productNew.setProductLine(pProductLine.getProductLine());
            //_productNew.setProducts(pDrink.getProducts());
            try {
                return ResponseEntity.ok(productLinesRepository.save(_productNew)); // lưu vào
            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("can not execute operation of this Entity" +
                                e.getCause().getCause().getMessage());
                // không thể thực thi hoạt động của Thực thể này
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
    // xóa các phần tử con mới xóa được cha
    public ResponseEntity<Object> deleteProductLineById(@PathVariable Long id) {
    try {
    productLinesRepository.deleteById(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
    System.out.println(e);
    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    }
    
}
