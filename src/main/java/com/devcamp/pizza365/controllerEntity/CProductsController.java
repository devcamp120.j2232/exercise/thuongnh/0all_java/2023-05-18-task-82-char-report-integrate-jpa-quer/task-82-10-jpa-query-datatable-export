package com.devcamp.pizza365.controllerEntity;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.serviceEntity.ProductEntityService;

@CrossOrigin
@RestController
@RequestMapping("/products")
public class CProductsController {

    @Autowired
    ProductEntityService pProductEntityService;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllUSer() {
       return pProductEntityService.getAllProduct();
    }

    @GetMapping("/details/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable long id) {
       return pProductEntityService.getProductById(id);
    }
    // JSON mẫu
    // "productCode": "S10_1678",
    // "productName": "1969 Harley Davidson Ultimate Chopper",
    // "productDescripttion": "This replica features working kickstand, front
    // suspension, gear-shift lever, footbrake lever, drive chain, wheels and
    // steering. All parts are particularly delicate due to their precise scale and
    // require special care and attention.",
    // "idProductLine": 2,
    // "productScale": "1:10",
    // "productVendor": "Min Lin Diecast",
    // "quantityInStock": 7933,
    // "buyPrice": 48.81

    @PostMapping("/create")
    public ResponseEntity<Object> createUser(@RequestBody Product paramProduct) {
       return pProductEntityService.createProduct(paramProduct);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable("id") Long id,
            @RequestBody Product paramProduct) {
                return pProductEntityService.updateProduct(id, paramProduct);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteUserById(@PathVariable Long id) {
       return pProductEntityService.deleteProductById(id);
    }

}
