package com.devcamp.pizza365.controllerEntity;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.serviceEntity.PaymentService;

@RestController
@CrossOrigin
@RequestMapping("/payment")
public class CPaymentController {

    @Autowired
	PaymentService paymentService;
    //  Viết code CRUD Rest API bảng payments(yêu cầu dùng service class).

      // 1 all
	  @GetMapping("/all")
	  public ResponseEntity<Object> getAllAlbum() {
		  return paymentService.getAllService();
	  }
  
	  // 2 thông tin user theo id
	  @GetMapping("/detail/{paramId}")
	  public ResponseEntity<Object> getAlbumById(@PathVariable Integer paramId) {
		  return paymentService.getAllByIdService(paramId);
	  }
  
	//   {
	// 
	// 	"lastName": "Berglund",
	// 	"firstName": "Christina",
	// 	"phoneNumber": "0921-12 3555",
	// 	"address": "Berguvsvägen 8",
	// 	"city": "Luleå",
	// 	"state": "null",
	// 	"postalCode": "S-958 22",
	// 	"country": "Sweden",
	// 	"salesRepEmployeeNumber": 1504,
	// 	"creditLimit": 53100
	// }
	  @PostMapping("/create")
	  public ResponseEntity<Object> createAlbum(@RequestBody Payment albumFormClient) {
		  return paymentService.createService(albumFormClient);
	  }
  
	  // 4 update
	  @PutMapping("/update/{id}")
	  public ResponseEntity<Object> updateUser(@PathVariable(name = "id") int paramId,
			  @RequestBody Payment albumFormClient) {
				  return paymentService.updateService(paramId, albumFormClient);
	  }
  
	  // 5 delete user //thì xóa luôn all orders thuộc user đó
	  @DeleteMapping("/delete/{id}")
	  public ResponseEntity<Object> deleteUser(@PathVariable int id) {
		  return paymentService.deleteService(id);
	  }
}
