package com.devcamp.pizza365.controllerEntity;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.serviceEntity.ProductLineEntitySevice;

@CrossOrigin
@RestController
@RequestMapping("/product_lines")
public class CProductLineController {

    @Autowired
    ProductLineEntitySevice productLineEntitySevice;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllDrink() {
       return productLineEntitySevice.getAllDrink();
    }

    @GetMapping("/details/{id}")
    public ResponseEntity<Object> getById(@PathVariable Long id) {
        return productLineEntitySevice.getById(id);

    }

    // create mẫu json thử
    // {
    // "id": 1,
    // "productLine": "Classic Cars",
    // "description": "Attention car enthusiasts: Make your wildest car ownership"
    // }
    @PostMapping("/create")
    public ResponseEntity<ProductLine> getCreate(@RequestBody ProductLine paramProductLine) {
       return productLineEntitySevice.getCreate(paramProductLine);
    }

    // update
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> getUpdate(@PathVariable("id") long id,
            @RequestBody ProductLine pProductLine) {
                return productLineEntitySevice.getUpdate(id, pProductLine) ;

       
    }
    // xóa các phần tử con mới xóa được cha
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteProductLineById(@PathVariable Long id) {
        return productLineEntitySevice.deleteProductLineById(id);
    }

}