package com.devcamp.pizza365.controllerEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.serviceEntity.OrderSeviceEntity;

@RestController
@CrossOrigin
@RequestMapping("/order_entity")
public class OrderEntityController {

    @Autowired
	OrderSeviceEntity pOrderSeviceEntity;
	// Viết code CRUD Rest API bảng customers (yêu cầu dùng service class).
	  // 1 all
	  @GetMapping("/all")
	  public ResponseEntity<Object> getAllAlbum() {
		  return pOrderSeviceEntity.getAllService();
	  }
  
	  // 2 thông tin user theo id
	  @GetMapping("/detail/{paramId}")
	  public ResponseEntity<Object> getAlbumById(@PathVariable Integer paramId) {
		  return pOrderSeviceEntity.getByIdService(paramId);
	  }
  
	//   {
	// 
	// 	"lastName": "Berglund",
	// 	"firstName": "Christina",
	// 	"phoneNumber": "0921-12 3555",
	// 	"address": "Berguvsvägen 8",
	// 	"city": "Luleå",
	// 	"state": "null",
	// 	"postalCode": "S-958 22",
	// 	"country": "Sweden",
	// 	"salesRepEmployeeNumber": 1504,
	// 	"creditLimit": 53100
	// }
	  @PostMapping("/create")
	  public ResponseEntity<Object> createAlbum(@RequestBody Order FormClient) {
		  return pOrderSeviceEntity.createService(FormClient);
	  }
  
	  // 4 update
	  @PutMapping("/update/{id}")
	  public ResponseEntity<Object> updateUser(@PathVariable(name = "id") int paramId,
			  @RequestBody Order FormClient) {
				  return pOrderSeviceEntity.updateService(paramId, FormClient);
	  }
  
	  // 5 delete user //thì xóa luôn all orders thuộc user đó
	  @DeleteMapping("/delete/{id}")
	  public ResponseEntity<Object> deleteUser(@PathVariable int id) {
		  return pOrderSeviceEntity.deleteService(id);
	  }
    
}
